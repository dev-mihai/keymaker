#!/bin/bash
#Waqas Abbas - 2023
serverUrl="<%= morpheus.applianceUrl %>"
accessToken="<%= morpheus.apiAccessToken %>"
morph_version="<%=customOptions.keyMakerMorpheusVersion%>"
cypherKey="<%=customOptions.keyMakerMorpCypherInput%>"
username="<%=customOptions.keyMakerMorpUsernameInput%>"
password='<%=customOptions.keyMakerMorpPasswordInput%>'
licenseKey="<%=results.myLicenseKey%>"
provisionType="<%=instance.provisionType%>"


app_url=""
status=""
lic_key=""
newAppAccessToken=""
lic_status=""
lic_selected=false
app_setup_failed=false
ui_down=false
timeout_initiated=false

echo "****Disable Unattended Upgrades****" >> /tmp/keymaker_log.txt 
yes Y | apt remove unattended-upgrades >> /tmp/keymaker_log.txt  

echo "****Download Morpheus****" >> /tmp/keymaker_log.txt 
wget "https://downloads.morpheusdata.com/files/morpheus-appliance_"$morph_version"_amd64.deb" >> /tmp/keymaker_log.txt  

#waiting for dpkg frontend file lock
sleep 600

echo "****Install Morpheus****" >> /tmp/keymaker_log.txt 
dpkg -i "morpheus-appliance_"$morph_version"_amd64.deb" >> /tmp/keymaker_log.txt  

#Change the morpheus.rb file to use the IP address instead of hostname for appliance url
internal_ip="<%= server.internalIp %>"
external_ip="<%= server.externalIp %>"
app_ip=$internal_ip
if [[ $provisionType == "azure"  ||  $provisionType == "amazon"  ||  $provisionType == "google" ]]; then
	app_ip=$external_ip
fi

cp /etc/morpheus/morpheus.rb /etc/morpheus/morpheus.rb.old 
rm /etc/morpheus/morpheus.rb
echo "appliance_url 'https://$app_ip'" >> /etc/morpheus/morpheus.rb

echo "****Reconfigure Morpheus****" >> /tmp/keymaker_log.txt 
morpheus-ctl reconfigure >> /tmp/keymaker_log.txt  


#Check if the UI is up
log_file="/var/log/morpheus/morpheus-ui/current"  #Morpheus current log file path
ui_started=false
timeout=900              #Set timeout in seconds for ui state check
seconds_counter=0

while ! $ui_started; #keep checking the log file for Morpheus ui banner until found
do
        if grep -Fq "****************************************" "$log_file"; then #Found UI banner
                ui_started=true
                echo "Morpheus UI is up" >> /tmp/keymaker_log.txt 
                sleep 20 #Wait 20 second before performing the initial setup
                #Initial Morpheus Setup
                config="/etc/morpheus/morpheus.rb"
                app_url=""
                while IFS= read -r line
                do
                        if [[ "$line" == *"appliance_url '"* ]]; then
                                app_url=$(echo "$line" | cut -d "'" -f2)
                                echo "App URL = $app_url" >> /tmp/keymaker_log.txt 
                        fi

                done < "$config"
                status=$(curl -XPOST "$app_url/api/setup" \
                        -H "accept: application/json" \
                        -H "Content-Type: application/json" \
                        -k \
                        -d '{
                        "hubmode": "skip",
                        "applianceName": "The Matrix",
                        "applianceUrl": "'$app_url'",
                        "accountName": "MasterTenant",
                        "firstName": "Morpheus",
                        "lastName": "Admin",
                        "username": "'$username'",
                        "email": "noemail@morpheusdata.com",
                        "password": "'$password'"
                        }' | python3 -c 'import sys, json; print(json.load(sys.stdin)["success"])')
                echo "Morpheus App Setup Status = $status" >> /tmp/keymaker_log.txt 
                if [[ $status == "True" ]]; then
                        echo "Try to add a license" >> /tmp/keymaker_log.txt 
                        if [[ "$cypherKey" == *"keymaker"* ]]; then
                                #echo "License to use = $cypherKey"
                                lic_key=$cypherKey
                                

                                sleep 5
                                #Get api access token from the new appliance
                                newAppAccessToken=$(curl -XPOST "$app_url/oauth/token?client_id=morph-api&grant_type=password&scope=write" \
                                -H "Content-Type: application/x-www-form-urlencoded" \
                                -k \
                                -d "username=$username" \
                                -d "password=$password" \
                                | python3 -c 'import sys, json; print(json.load(sys.stdin)["access_token"])')

                                # Check if newAppAccessToken has been set
                                if [[ -z "$newAppAccessToken" ]]; then
                                    echo "Error: newAppAccessToken is empty." >> /tmp/keymaker_log.txt
                                    # Handle the error or exit the script
                                    exit 1
                                fi

                                sleep 5
                                #Apply license
                                response=$(curl -XPOST "$app_url/api/license" \
                                -H "Authorization: BEARER $newAppAccessToken" \
                                -H "Content-Type: application/json" \
                                -k \
                                -d '{
                                        "license": "'$licenseKey'"
                                
                                    }' | python3 -c 'import sys, json; print(json.load(sys.stdin))')
                                #     echo "newApiToken=$newAppAccessToken"
                                      echo "License status = $response" >> /tmp/keymaker_log.txt 
                                lic_status=$response
                        else
	                        echo "No license selected" >> /tmp/keymaker_log.txt 
                                lic_selected=false
                        fi
                else
                        app_setup_failed=true
                        echo "Appliance Setup Failed" >> /tmp/keymaker_log.txt 
                        echo "Please check the security groups/firewall rule on the cloud to make sure the IP $app_url is accessible over port 443." >> /tmp/keymaker_log.txt 
                fi
                
        else    
                ui_down=true
                echo "UI is down" >> /tmp/keymaker_log.txt 
        fi
        sleep 1 #Wait 1 second before checking again
        ((seconds_counter++))
        if ((seconds_counter >= timeout)); then #If timeout reached then end log file check
                echo "timeout initiated" >> /tmp/keymaker_log.txt 
                timeout_initiated=true
                break
        fi
done



echo "app_url=$app_url,newAppAccessToken=$newAppAccessToken"
#Install Virtualenv
echo "****Install Virtualenv****" >> /tmp/keymaker_log.txt
DEBIAN_FRONTEND=noninteractive apt-get -y install virtualenv >> /tmp/keymaker_log.txt
