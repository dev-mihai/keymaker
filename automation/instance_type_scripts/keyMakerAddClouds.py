import json
import requests
from cryptography.fernet import Fernet

encoding='utf-8'
newApplianceUrl=str(morpheus['results']['installStatus']['app_url']).strip()
newApplianceAccessToken=str(morpheus['results']['installStatus']['newAppAccessToken']).strip()
serverUrl=morpheus['morpheus']['applianceUrl']
accessToken=morpheus['morpheus']['apiAccessToken']
cloudCredsKeyFromCypher=morpheus['customOptions']['keyMakerAddCloudsInput']

print('new appliance url = '+str(newApplianceUrl))
print('new appliance access token = '+str(newApplianceAccessToken))

#Get cloud credentials from the cypher
creds=[]
for key in cloudCredsKeyFromCypher:
    response = requests.get(serverUrl+"/api/cypher/"+key,
    headers = {
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Authorization": "Bearer "+accessToken,
        "Accept-Encoding": None
        },
    verify = False)
    cred=response.json()['data']
    cloudType = key.split("/")[4]
    cloudlName = key.split("/")[5]
    cloudCred={'cloud_type':cloudType,
                'cloud_name':cloudlName,
                'credentials':cred}
    
    creds.append(cloudCred)

cloudsCreds=creds


keyChain=''

response = requests.get(serverUrl+"/api/cypher/secret/keymaker/keychain",
headers = {
    "Content-Type": "text/plain",
    "Accept": "*/*",
    "Authorization": "Bearer "+accessToken,
    "Accept-Encoding": None
    },
verify = False)

if response.status_code==404:
	print('keyChain not found')

else:
    keyChain=response.json()['data']['key'].encode(encoding)
    fernet = Fernet(keyChain)
    print(json.dumps(cloudsCreds))

    #Create Group
    payload = {"group": {"name": "keyMaker"}}
    response = requests.post(newApplianceUrl+"/api/groups",json=payload,
    headers = {
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Authorization": "Bearer "+newApplianceAccessToken,
        "Accept-Encoding": None
        },
    verify = False)
    print(response.json())
    group_id=response.json()['group']['id']

    for credential in cloudsCreds:
        d={}
        if credential['cloud_type'] == "azure":
            #Add Azure Cloud
            subscription_id = fernet.decrypt(credential['credentials']['subscription_id']).decode()
            tenant_id =fernet.decrypt(credential['credentials']['tenant_id']).decode()
            client_id =fernet.decrypt(credential['credentials']['client_id']).decode()
            client_secret =fernet.decrypt(credential['credentials']['client_secret']).decode()
            d={
                'zone':{
                    'name': credential['cloud_name'],
                    'code': credential['cloud_name'],
                    'description': 'Created by keyMaker automation',
                    'location': 'Azure',
                    'zoneType': {'code': 'azure'},
                    'accountId': 1,
                    'groupId':group_id,
                    'config': {
                        'cloudType': 'global',
                        'subscriberId': subscription_id,
                        'tenantId': tenant_id,
                        'clientId': client_id,
                        'clientSecret': client_secret,
                        }
                    }
            }
            
    
        elif credential['cloud_type'] == "vmware": #Add VCenter Cloud

            vmware_api_url = fernet.decrypt(credential['credentials']['vmware_api_url']).decode()
            vmware_username = fernet.decrypt(credential['credentials']['vmware_username']).decode()
            vmware_password = fernet.decrypt(credential['credentials']['vmware_password']).decode()
    
            d={
            	'zone':{
                      'name': credential['cloud_name'],
                      'zoneType': {'code': 'vmware'},
                      'accountId': 1,
                      'groupId':group_id,
                      'config': {
                          'apiUrl': vmware_api_url,
                            'username': vmware_username,
                            'password': vmware_password,
                            'apiVersion': '7.0',
                            'datacenter': 'labs-den-dc2-demo',
                            'cluster': 'Demo',
                            'resourcePoolId': 'resgroup-38512',
                            'rpcMode': 'guestexec',
                            'enableVnc': 'on',
                      	  }
                     }
              }

        elif credential['cloud_type'] == "amazon": #Add AWS Cloud

            amazon_region = fernet.decrypt(credential['credentials']['amazon_region']).decode()
            amazon_access_key = fernet.decrypt(credential['credentials']['amazon_access_key']).decode()
            amazon_secret_key = fernet.decrypt(credential['credentials']['amazon_secret_key']).decode()
    
            d={
            	'zone':{
                      'name': credential['cloud_name'],
                      'zoneType': {'code': 'amazon'},
                      'accountId': 1,
                      'groupId':group_id,
                      'config': {
                            'endpoint': amazon_region,
                            'accessKey': amazon_access_key,
                            'secretKey': amazon_secret_key,
                            'isVpc': 'true',
                      	  }
                     }
              }
        
        elif credential['cloud_type'] == "google": #Add GCP Cloud

            google_client_email = fernet.decrypt(credential['credentials']['google_client_email']).decode()
            google_private_key = fernet.decrypt(credential['credentials']['google_private_key']).decode()
            google_project_id = fernet.decrypt(credential['credentials']['google_project_id']).decode()
            google_region = fernet.decrypt(credential['credentials']['google_region']).decode()

    
            d={
            	'zone':{
                      'name': credential['cloud_name'],
                      'zoneType': {'code': 'googlecloud'},
                      'accountId': 1,
                      'groupId':group_id,
                      'config': {
                            'privateKey': google_private_key,
                            'clientEmail': google_client_email,
                            'projectId': google_project_id,
                            'googleRegionId': google_region,
                      	  }
                     }
              }

        myData=json.dumps(d).encode('utf-8')
        response = requests.post(newApplianceUrl+"/api/zones",
        headers = {
            "Content-Type": "text/plain",
            "Accept": "*/*",
            "Authorization": "Bearer "+newApplianceAccessToken,
            "Accept-Encoding": None
            },
        data = myData,
        verify = False)
        print(response.json())