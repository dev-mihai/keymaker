cypherKey="<%=customOptions.keyMakerMorpCypherInput%>"
serverUrl="<%= morpheus.applianceUrl %>"
accessToken="<%= morpheus.apiAccessToken %>"

trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"
    printf '%s' "$var"
}


if [[ "$cypherKey" == *"keymaker"* ]]; then
    licenseKey=$(curl "$serverUrl/api/cypher/$cypherKey" \
                                    -H "Authorization: BEARER $accessToken" \
                                    -H "Content-Type: application/json" \
                                    -k \
                                    | python3 -c 'import sys, json; print(json.load(sys.stdin)["data"])')

	trim "$licenseKey"
else
	trim "No license selected"
fi